var request = require('request'); // For external requests
var express = require('express'); // For routing
var https = require('https');
module.exports = function(app) {

	// api
	// get all users
	app.get('/api/users', function(req, res) {
		request('https://shadow-python.herokuapp.com/users', function(error, response, body) {
	    if (!error && response.statusCode == 200) {
					var users = JSON.parse(body);
					res.json(users);
	    } else {
	      console.log(error);
	    }
	  })
	});

	// Create user and send back all users after creation
	app.post('/api/users', function(req, res) {
		var new_user = req.body
		var options = {
      uri: 'https://shadow-python.herokuapp.com/user',
      method: 'POST',
      json: new_user
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
	});

	// TODO: Delete user from view
	app.delete('/api/users/:user_id', function(req, res) {	});

	// get all rendez-vous(rdv)
	app.get('/api/rdvs', function(req, res) {
		request('https://shadow-python.herokuapp.com/rdvs', function(error, response, body) {
	    if (!error && response.statusCode == 200) {
					var rdvs = JSON.parse(body);
					res.json(rdvs);
	    } else {
	      console.log(rdvs);
	    }
	  })
	});

	// Create rdv and send back all rdvs after creation
	app.post('/api/rdvs', function(req, res) {
		var new_rdv = req.body
		console.log(new_rdv);
		var options = {
      uri: 'https://shadow-python.herokuapp.com/rdv',
      method: 'POST',
      json: new_rdv
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
	});

	// TODO: Delete rendez-vous(rdv) from view
	app.delete('/api/rdvs/:rdv_id', function(req, res) {	});

	// get all notaires
	app.get('/api/notaires', function(req, res) {
		request('https://shadow-python.herokuapp.com/notaires', function(error, response, body) {
	    if (!error && response.statusCode == 200) {
					var notaires = JSON.parse(body);
					res.json(notaires);
	    } else {
	      console.log(notaires);
	    }
	  })
	});

	// Create notaire and send back all notaires after creation
	app.post('/api/notaires', function(req, res) {
		var new_notaire = req.body
		console.log(new_notaire);
		var options = {
      uri: 'https://shadow-python.herokuapp.com/notaire',
      method: 'POST',
      json: new_notaire
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
	});

	// TODO: Delete notaire from notaires
	app.delete('/api/notaires/:notaire_id', function(req, res) {	});

	// get all admins
	app.get('/api/admins', function(req, res) {
	  request('https://shadow-python.herokuapp.com/admins', function(error, response, body) {
	    if (!error && response.statusCode == 200) {
	        var admins = JSON.parse(body);
	        res.json(admins);
	    } else {
	      console.log(admins);
	    }
	  })
	});

	// Create admin and send back all admins after creation
	app.post('/api/admins', function(req, res) {
	  var new_admin = req.body
	  console.log(new_admin);
	  var options = {
	    uri: 'https://shadow-python.herokuapp.com/admin',
	    method: 'POST',
	    json: new_admin
	  };

	  request(options, function (error, response, body) {
	    if (!error && response.statusCode == 200) {
	      console.log(body)
	    }
	  });
	});

	// TODO: Delete admin from admins
	app.delete('/api/admins/:admin_id', function(req, res) {	});

	app.get('*', function(req, res) {
		res.sendfile('./public/index.html'); // load the single view file to allow angular to handle the page changes on the front-end
	});
};
