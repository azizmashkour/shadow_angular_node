var express  = require('express');
var app      = express();
var port  	 = process.env.PORT || 8888;

var bodyParser = require('body-parser'); 	// pull information from HTML POST
var methodOverride = require('method-override'); // simulate DELETE and PUT

app.use(express.static(__dirname + '/public')); 				// set the static files location /public/img will be /img for users
app.use(bodyParser.urlencoded({'extended':'true'})); 			// parse application/x-www-form-urlencoded
app.use(bodyParser.json()); 									// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// load App routes server side
require('./app/routes.js')(app);

// listen (start app with node server.js)
app.listen(port);
console.log("App listening on port " + port);
