var shadowApp = angular.module('shadowApp', []);

function mainController($scope, $http) {
	$scope.formData = {};

	// when landing on the page, get all users and show them
	$http.get('/api/users')
		.success(function(data) {
			$scope.users = data['result'];
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	// when submitting the add form, send the text to the node API
	$scope.createUser = function() {
		$http.post('/api/users', $scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				$scope.users = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	$scope.deleteUser = function(id) {
		$http.delete('/api/users/' + id)
			.success(function(data) {
				$scope.users = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	// when landing on the page, get all rdvs and show them
	$http.get('/api/rdvs')
		.success(function(data) {
			$scope.rdvs = data['result'];
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	// when submitting the add form, send the text to the node API
	$scope.createRdv = function() {
		$http.post('/api/rdvs', $scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				$scope.rdvs = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	$scope.deleteRdv = function(id) {
		$http.delete('/api/rdvs/' + id)
			.success(function(data) {
				$scope.rdvs = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	// when landing on the page, get all notaires and show them
	$http.get('/api/notaires')
		.success(function(data) {
			$scope.notaires = data['result'];
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	// when submitting the add form, send the text to the node API
	$scope.createNotaire = function() {
		$http.post('/api/notaires', $scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				$scope.notaires = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	$scope.deleteNotaire = function(id) {
		$http.delete('/api/notaires/' + id)
			.success(function(data) {
				$scope.notaires = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	// when landing on the page, get all admins and show them
	$http.get('/api/admins')
	  .success(function(data) {
	    $scope.admins = data['result'];
	  })
	  .error(function(data) {
	    console.log('Error: ' + data);
	  });

	// when submitting the add form, send the text to the node API
	$scope.createAdmin = function() {
	  $http.post('/api/admins', $scope.formData)
	    .success(function(data) {
	      $scope.formData = {}; // clear the form so our user is ready to enter another
	      $scope.admins = data;
	      console.log(data);
	    })
	    .error(function(data) {
	      console.log('Error: ' + data);
	    });
	};

	$scope.deleteNotaire = function(id) {
	  $http.delete('/api/admins/' + id)
	    .success(function(data) {
	      $scope.admins = data;
	    })
	    .error(function(data) {
	      console.log('Error: ' + data);
	    });
	};

}
